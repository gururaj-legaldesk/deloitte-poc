import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TemplatesComponent }      from './templates.component';
import { TemplatesRoutingModule }  from './templates-routing.module';
import { TemplateService } from '../services/template.service';

@NgModule({
  imports: [
    TemplatesRoutingModule,CommonModule,FormsModule
  ],
  declarations: [ TemplatesComponent ],
  providers: [ 
    TemplateService
  ]
})
export class TemplatesModule { }
