import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'esign',
        loadChildren: './esign/esign.module#EsignModule'
      },
      {
        path: 'documents',
        loadChildren: './documents/documents.module#DocumentsModule'
      },
      {
        path: 'my_templates',
        loadChildren: './templates/templates.module#TemplatesModule'
      },
       {
        path: 'all_templates',
        loadChildren: './templates/templates.module#TemplatesModule'
      },
      {
        path: 'workflow',
        loadChildren: './workflow/workflow.module#WorkflowModule'
      },
      {
        path: 'activity-logs',
        loadChildren: './activity_log/activity_log.module#ActivityLogModule'
      },
      {
        path: 'authorities',
        loadChildren: './authorities/authorities.module#AuthoritiesModule'
      },
       {
        path: 'users',
        loadChildren: './authorities/authorities.module#AuthoritiesModule'
      },

       {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      },
       {
        path: 'help',
        loadChildren: './help/help.module#HelpModule'
      },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      },
      {
        path: 'icons',
        loadChildren: './icons/icons.module#IconsModule'
      },
      {
        path: 'forms',
        loadChildren: './forms/forms.module#FormsModule'
      },
      {
        path: 'plugins',
        loadChildren: './plugins/plugins.module#PluginsModule'
      },
      {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'charts',
        loadChildren: './chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'uikits',
        loadChildren: './uikits/uikits.module#UIKitsModule'
      }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  },
  {
    path: 'online',
    component: SimpleLayoutComponent,
    data: {
      title: 'eSign'
    },
    children: [
      {
        path: '',
        loadChildren: './documents/documents.module#DocumentsModule',
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
