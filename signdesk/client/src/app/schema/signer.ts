export class Signer{
  document_id :string;
  signer_index: number;
  external_user:boolean
  user_id: string;
  signature_sequence:number
  email_notification:boolean
  signature_page: string;
  signature_position :Object;
  status: string;
  signed_at:Date;
  invited_at:Date;
  invited_by:string;
  remainders :Object
}
