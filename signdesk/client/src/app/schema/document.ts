export class Document {
  organisation_id:string;
  department_id:string;
  document_name:string;
  created_by:string;
  created_at:Date;
  updated_by:string;
  updated_at:Date;
  document_title:string;
  document_path:string;
  document_source:string;
  is_deleted:boolean;
  signature_sequence:string;
  enable_email_notification:boolean;
  suporting_documents:Object;
  latest_document:string;
}
