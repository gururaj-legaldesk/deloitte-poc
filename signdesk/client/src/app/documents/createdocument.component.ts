import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';
import { EsignService } from '../services/esign.service';
import { FileUploader } from 'ng2-file-upload';
import { Document } from '../schema/document';
import { Signer } from '../schema/signer';

import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';

@Component({
  templateUrl: 'createdocument.component.html'
})

export class CreateDocumentComponent implements OnInit {

  public sourceType:string   =  '';
  public currentStep:string  =  'step1';
  public newDocumentObj      = new Document();
  public newSignerObj        = new Signer();

  public signer_name:string  =  '';
  public signer_email:string =  '';
  public signer_mobile:number;

  public signerList          = []

  signer_aadhaar:string;
  signer_otp:string;

  // Set our default values
  importOption =false
  localState = { value: '' };
  fileName ='';
  askAadhaar:boolean;
  askAadhaarOTP =false;


  aadhaarError :boolean;
  aadhaarOtpError :boolean;
  eSignResponse: boolean;
  termsandconditions :boolean;
  esignedFileSource :string;
  uploadedFileName:string;
  transactionId:string;
  loading:boolean; otpError:boolean;
  documentId:string;
  self_esign:boolean =false;

  private toasterService: ToasterService;

  public toasterconfig : ToasterConfig =
    new ToasterConfig({
      tapToDismiss: true,
      timeout: 5000
    });


  public uploader:FileUploader = new FileUploader({url:'http://localhost:4000/upload'});

  public excelUploader:FileUploader = new FileUploader({url:'http://localhost:4000/excelUpload'});
 // public excelUploader:FileUploader = new FileUploader({url:'http://localhost:4000/api/esign/excelUpload'});

  // TypeScript public modifiers
  constructor( private router: Router ,private http:Http,private esignService:EsignService,toasterService: ToasterService) {
    this.sourceType  = "";
    this.currentStep = 'step1';
    this.signer_aadhaar=  "";
    this.toasterService = toasterService;
  }


  fileEvent(fileInput: any){
    let file = fileInput.target.files[0];
    this.fileName = file.name;


    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e : any) {
        (<HTMLInputElement>document.getElementById('file_preview')).src= e.target.result;

      }
      reader.readAsDataURL(fileInput.target.files[0]);


    }
  }

  getSignedDocument(){
    if(!this.signer_aadhaar) {
      this.aadhaarError = true
    }else if(this.signer_aadhaar && this.signer_aadhaar.toString().length<4){
      this.aadhaarError  = true
    }else{
      this.aadhaarError  = false
      this.loading       = true

      this.esignService.getTriggarOTP(this.signer_aadhaar)
        .subscribe(response => {
          var responseJson     = JSON.parse(response);
          this.loading         = false

          if(responseJson.ErrorCode=="NA") {
            this.currentStep   = 'step5';
            this.transactionId = responseJson.Transaction_Id;
            this.otpError      = false
          }else {
            this.currentStep   = 'step4';
            this.otpError      = true
            this.loading       = false
          }
        },
        err => {
          this.currentStep   = 'step4';
          this.otpError      = true
          this.loading       = false
        }

        );
    }
  }

  validateAadhaar(){
    if(!this.signer_aadhaar) {
      this.aadhaarError = true
    }else if(this.signer_aadhaar && this.signer_aadhaar.toString().length<4){
      this.aadhaarError  = true
    }else{
      this.aadhaarError  = false
      this.loading       = true


      this.esignService.getTriggarOTP(this.signer_aadhaar)
        .subscribe(response => {
          var responseJson     = JSON.parse(response);
          this.loading         = false

          if(responseJson.ErrorCode=="NA") {
            this.currentStep   = 'step5';
            this.transactionId = responseJson.Transaction_Id;
            this.otpError      = false
          }else {
            this.currentStep   = 'step4';
            this.otpError      = true
            this.loading       = false
          }
        },
          err => {
          this.currentStep   = 'step4';
          this.otpError      = true
          this.loading       = false
        }

      );


    }
  }


  validateOTP() {

    if (!this.signer_otp) {
      this.aadhaarOtpError  = true
    } else if (this.signer_otp && this.signer_otp.toString().length < 4) {
      this.aadhaarOtpError  = true
    } else if(!this.termsandconditions){
      this.aadhaarOtpError  = true
    }else{
      this.aadhaarOtpError  = false

      //invoke Esign api here
      this.loading       = true
      var params= {
        aadhaar: this.signer_aadhaar,
        otp :this.signer_otp,
        transactionId: this.transactionId, //"c5292174c75739ba7b8fb9898a0c854c",, //this.transactionId,
        file: "uploads/"+ this.uploadedFileName,
        referenceId:"signdesk"+Date.now().toString(), //this.transactionId
        outputFile: "signed/signed_"+this.uploadedFileName
      }


     // return  this.http.get('http://localhost:4000/api/esign/getSigned/'+argument.aadhaar+'/'+argument.otp+'/'+argument.transactionId+'/'+argument.referenceId+'/'+argument.file+'/'+argument.outputFile)

      this.esignService.getSignedDocument(params)
        .subscribe(response => {
          console.log(response)

          this.loading       = false
          this.askAadhaarOTP = false

          this.currentStep   = 'step6';

          if(response.toString().trim()=="Signed") {
            this.otpError          = false
            this.esignedFileSource ='http://localhost:4000/api/esign/download/signed_'+this.uploadedFileName;

            this.esignService.notifySignerByOrder({
              documentId:this.documentId
            })
              .subscribe(response => {
            });

            var dataParams= {
              documentId : this.documentId,
              filename   : "signed_"+this.uploadedFileName
            }
            this.esignService.updateFilename(dataParams)
              .subscribe(response => {
            });


          }else {



            this.askAadhaarOTP = false
            this.otpError      = true
            this.loading       = false
          }
        });
    }
  }


  saveDocument(event){
    event.preventDefault();

    if(this.self_esign){
      this.currentStep = 'step4';
    }else{
      //trigger email
      this.currentStep = 'step3';
      var params= {
         documentId:this.documentId
      }

      this.esignService.notifySignerByOrder(params)
        .subscribe(response => {

      });



    }

  }

  newDocument() {
    this.askAadhaar    = true
    this.askAadhaarOTP = false
    this.eSignResponse = false
    this.fileName      = ''
    this.signer_otp    = ''
    this.signer_aadhaar       = ''
    this.termsandconditions   = false
  }

  downloadFile() {
    //  this.esignService.downloadEsignedFile(this.fileName )
  }

  uploadFile(item) {
      var resp;
      resp = item.upload();

      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
          var responsePath = JSON.parse(response);
          this.askAadhaar = true;
          this.uploadedFileName =responsePath.fileInfo.filename

          var params = {
            userId     : '1000',
            fileName   : responsePath.fileInfo.filename,
            source     : 'local-file'
          }

          this.newDocumentObj.organisation_id            = 'org_1'
          this.newDocumentObj.department_id              = 'dep_1'
          this.newDocumentObj.document_name              = responsePath.fileInfo.filename
          this.newDocumentObj.created_by                 = 'user_1'
          this.newDocumentObj.updated_by                 = 'user_1'
          this.newDocumentObj.document_title             = responsePath.fileInfo.filename
          this.newDocumentObj.document_path              = "uploads/"
          this.newDocumentObj.document_source            = 'local'
          this.newDocumentObj.is_deleted                 = false
          this.newDocumentObj.signature_sequence         = 'sequence'   //parallel
          this.newDocumentObj.enable_email_notification  = false
          this.newDocumentObj.created_at                 = new Date()


          this.esignService.createDocument(this.newDocumentObj)
            .subscribe(response => {
              this.documentId  =response._id
              console.log( this.documentId )
              this.currentStep = 'step2';
              if(response.toString().trim()=="Signed") {
                this.documentId =response.toString().trim()
              }
          });
      };
  }



  importFile(item) {
    var resp;



    resp = item.upload();

    this.excelUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        var responsePath = JSON.parse(response);

      var importInfo  = {
        filename  : responsePath.fileInfo.filename,
        documentId :this.documentId ,
      }

       this.esignService.importSigner(importInfo)
        .subscribe(response => {

          for (var i=0; i<response.length; i++){
            this.signerList.push(response[i]);
          }

          this.importOption =false;
          item.cancel()
          this.excelUploader.clearQueue()
          this.toasterService.pop('success', 'Successful', 'Imported Successfully.');
        });

    };

  }



  addSigner(event){
    event.preventDefault();

    this.newSignerObj.document_id      = this.documentId
    this.newSignerObj.signer_index     = 1
    this.newSignerObj.external_user    = false
    this.newSignerObj.user_id          = ''
    this.newSignerObj.signature_sequence = 1
    this.newSignerObj.email_notification = false
    this.newSignerObj.signature_page     = 'all' //
    this.newSignerObj.signature_position = {
      page     : 'all',
      position : 'bottom-left'
    }
    this.newSignerObj.status              = "invited"
    this.newSignerObj.invited_at          = new Date()
    this.newSignerObj.invited_by          = 'user_1'
    this.newSignerObj.remainders          = {}


    var signerData = {
      signer_name: this.signer_name,
      signer_email: this.signer_email,
      signer_mobile: this.signer_mobile,
      signerInfo: this.newSignerObj
    }


    this.esignService.addSigner(signerData)
      .subscribe(response => {

        this.signerList.push(response)
        this.signer_name =  '';
        this.signer_email=  '';
        this.signer_mobile =null
        console.log(response)
     });


  }


  deleteSigner(signer){
      var signerList = this.signerList;

      this.esignService.deleteSigner(signer._id).subscribe(data => {

        this.toasterService.pop('success', 'Successful', 'Signer Remove Successfully.');
        console.log('data',data)
        if(data.n == 1){
          for(var i = 0;i < signerList.length;i++){
            if(signerList[i]._id == signer._id){
              signerList.splice(i, 1);
            }
          }
        }
      });
   }

  /*
    this.esignService.sendEmail({})
  .subscribe(response => {
    console.log(response)
  });
  */




    ngOnInit(): void {
    //generate random values for mainChart

  // this.currentStep = 'step5';

  }
}
