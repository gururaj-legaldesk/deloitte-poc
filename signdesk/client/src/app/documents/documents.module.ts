import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// Notifications
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import { DocumentsRoutingModule }  from './documents-routing.module';

import { DocumentService }         from '../services/document.service';
import { EsignService }            from '../services/esign.service';

import { DocumentsComponent }      from './documents.component';
import { CreateDocumentComponent } from './createdocument.component';
import { ViewDocumentComponent } from './viewdocument.component';
import { SignDocumentComponent } from './signdocument.component';

import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

@NgModule({
  imports: [
    DocumentsRoutingModule,CommonModule,FormsModule,ToasterModule
  ],
  declarations: [ DocumentsComponent,CreateDocumentComponent,FileSelectDirective,ViewDocumentComponent,SignDocumentComponent],
  providers: [
    DocumentService,
    EsignService
  ]
})
export class DocumentsModule { }
