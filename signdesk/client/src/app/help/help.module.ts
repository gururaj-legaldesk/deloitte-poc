import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HelpComponent }      from './help.component';
import { HelpRoutingModule }  from './help-routing.module';
import { HelpService } from '../services/help.service';

@NgModule({
  imports: [
    HelpRoutingModule,CommonModule,FormsModule
  ],
  declarations: [ HelpComponent ],
  providers: [ 
    HelpService
  ]
})
export class HelpModule { }
