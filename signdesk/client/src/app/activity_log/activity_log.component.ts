import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityLogService } from '../services/activity_log.service';
import { Http } from '@angular/http';

@Component({
  templateUrl: 'activity_log.component.html'
})


export class ActivityLogComponent {

  public data;
  public filterQuery = '';

  constructor(private http:Http) {
    http.get('data.json')
      .subscribe((data)=> {
        setTimeout(()=> {
          this.data = data.json();
        }, 2000);
      });
}

  public toInt(num:string) {
    return +num;
  }

  public sortByWordLength = (a:any) => {
    return a.name.length;
  }
}
