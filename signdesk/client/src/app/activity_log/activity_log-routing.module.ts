import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityLogComponent } from './activity_log.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityLogComponent,
    data: {
      title: 'Activity Log'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityLogRoutingModule {}
