import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProfileComponent }      from './profile.component';
import { ProfileRoutingModule }  from './profile-routing.module';
import { ProfileService } from '../services/profile.service';
import { DataTableModule } from 'angular2-datatable';
import { DataFilterPipe } from './datafilterpipe';


import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    ProfileRoutingModule,CommonModule,FormsModule,TabsModule,DataTableModule
  ],
  declarations: [ ProfileComponent,DataFilterPipe ],
  providers: [ 
    ProfileService
  ]
})
export class ProfileModule { }
