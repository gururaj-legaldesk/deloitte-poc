import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { WorkflowService } from '../services/workflow.service';



@Component({
  templateUrl: 'workflow.component.html'
})
export class WorkflowComponent implements OnInit {

  public brandPrimary:string =  '#20a8d8';
  public brandSuccess:string =  '#4dbd74';
  public brandInfo:string =   '#67c2ef';
  public brandWarning:string =  '#f8cb00';
  public brandDanger:string =   '#f86c6b';

  // Set our default values
  localState = { value: '' };

  loading:boolean;

userId:string;



  // TypeScript public modifiers
  constructor( private router: Router ,private http:Http,private workflowService:WorkflowService) {
    this.userId=  "";
  }

  ngOnInit(): void {
    //generate random values for mainChart
/*

this.documentService.getDocuments(this.userId)
        .subscribe(response => {

          var responseJson = JSON.parse(response);
          this.loading       = false

          console.log('apiResponse error',responseJson.Error)
          if(responseJson.ErrorCode=="NA") {
          }else {
            this.loading   = false
          }
        });
*/


  }
}
