import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class EsignService{
    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getTriggarOTP(aadhaar){

/*
       return  this.http.get('http://localhost:4000/api/esign/requestOTP/'+aadhaar).map((response:Response) => {

           console.log('response clarification',response)
           return response.json();
        }).subscribe();
*/
        return this.http.get('http://localhost:4000/api/esign/requestOTP/'+aadhaar)
            .map(res => res.json())
           // .catch((error:any) => console.log(error.json().error || 'Server error'));
}




    getSignedDocument(argument){

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:4000/api/esign/getSigned', JSON.stringify(argument), {headers: headers})
          .map(res => res.json());

     /*   return  this.http.get('http://localhost:4000/api/esign/getSigned/'+argument.aadhaar+'/'+argument.otp+'/'+argument.transactionId+'/'+argument.referenceId+'/'+argument.file+'/'+argument.outputFile)
            .map(res => res.json());*/
    }

    updateFilename(params){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:4000/api/esign/updateFilename', JSON.stringify(params), {headers: headers})
          .map(res => res.json());
    }

    getDocumentInfo(documentId){
       return  this.http.get('http://localhost:4000/api/esign/fetchDocument/'+documentId)
        .map(res => res.json());
    }

    getDocumentSingerInfo(documentId){
      return  this.http.get('http://localhost:4000/api/esign/fetchDocumentSigner/'+documentId)
        .map(res => res.json());
    }

    createDocument(params) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:4000/api/esign/createDocument/', JSON.stringify(params), {headers: headers})
          .map(res => res.json());
    }

    addSigner(signerData){
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post('http://localhost:4000/api/esign/addSigner', JSON.stringify(signerData), {headers: headers})
        .map(res => res.json());
    }


    importSigner(importInfo){
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post('http://localhost:4000/api/esign/importSigner', JSON.stringify(importInfo), {headers: headers})
        .map(res => res.json());
    }


    deleteSigner(id){
      return this.http.delete('http://localhost:4000/api/esign/deleteSigner/'+id)
        .map(res => res.json());
    }



   notifySignerByOrder(params){
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post('http://localhost:4000/api/esign/notifySignerByOrder/', JSON.stringify(params), {headers: headers})
        .map(res => res.json());
    }


  sendEmail(params){
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post('http://localhost:4000/api/esign/sendEmail/', JSON.stringify(params), {headers: headers})
        .map(res => res.json());
    }


  getTasks(){
        return this.http.get('/api/tasks')
            .map(res => res.json());
    }

    addTask(newTask){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/api/task', JSON.stringify(newTask), {headers: headers})
            .map(res => res.json());
    }


    updateStatus(task){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('/api/task/'+task._id, JSON.stringify(task), {headers: headers})
            .map(res => res.json());
    }


}
