import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class DocumentService{
    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getDocuments(){
      return this.http.get('http://localhost:4000/api/esign/getDocuments/')
        .map(res => res.json());
    }

}
