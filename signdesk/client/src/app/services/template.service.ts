import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class TemplateService{
    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getTemplates(userId){
      
        return this.http.get('http://localhost:4000/api/esign/dashboard/'+userId)
            .map(res => res.json());
    }

}
