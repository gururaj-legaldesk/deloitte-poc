/**
 * Created by gururaj on 4/17/17.
 */
var express   = require('express');
var router    = express.Router();
var mongojs   = require('mongojs');
var http      = require('http'),
    url       = require('url');

var fs        = require('fs');
var path      = require('path');
var mime      = require('mime');
var xls = require('excel');


var multer        = require('multer');

//var db      = mongojs('mongodb://brad:brad@ds047666.mlab.com:47666/mytasklist_brad', ['tasks']);
var db        = mongojs('mongodb://localhost:27017/signdesk', ['signdesk_documents','signdesk_users','signdesk_signers']);
var ObjectId  = mongojs.ObjectId;
var emailUser = "gururaj.shetty"
var auth      = 'leg@lDe5k'

const nodemailer = require('nodemailer');

// Get All Tasks
router.get('/requestOTP/:aadhaar', function(req, res, next){

try{
    console.log('req.params.aadhaar',req.params.aadhaar);

    var aadharNo= req.params.aadhaar.toString();   //914156057818
    var today   = new Date();
    /* var options = {
        host: 'https://signdesk.com',
        port: 8080,
        path: '/esignService/api/staging/otpCall',
        headers:{"X-Parse-Application-Id":"rakesh","X-Parse-REST-API-Key":"q1w2e3r4","Content-Type":"application/json"},
        json: true,
        method: 'POST'
    };*/

    var opts = url.parse('http://35.154.135.90:8080/esignapp/api/staging/otpCall');

    opts.headers = {"X-Parse-Application-Id":"rakesh","X-Parse-REST-API-Key":"q1w2e3r4","Content-Type":"application/json"};
    opts.json=true;
    opts.method= "POST";


    var httprequest= http.request(opts, function(signdeskResponse) {

        var finalResult = '';

        //another chunk of data has been recieved, so append it to `str`
        signdeskResponse.on('data', function (chunk) {
            finalResult += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        signdeskResponse.on('end', function () {
            console.log(finalResult);


           var jsonObject = JSON.parse(finalResult);
            if(jsonObject.ErrorCode==='NA'){
                res.status(200);
                res.json(finalResult);
            } else{
                res.status(400);
                res.json(finalResult);
            }
        });

    });

    var data = {"AadhaarNo":aadharNo,"Reference_Id":Date.now().toString(),"TimeStamp":today.toISOString().toString()};
    httprequest.end(JSON.stringify(data));

}catch(e){
    console.log(e)
}

});

router.get('/download/:esignedFile', function(req, res){
    var file = 'signed/'+ req.params.esignedFile;
    var filename = path.basename(file);
    var mimetype = mime.lookup(file);
    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(file);
    filestream.pipe(res);
});


router.get('/download/original/:esignedFile', function(req, res){
    var file = 'uploads/'+ req.params.esignedFile;
    var filename = path.basename(file);
    var mimetype = mime.lookup(file);
    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(file);
    filestream.pipe(res);
});




router.get('/viewfile/:filename', function(req, res){
    var file = 'uploads/'+ req.params.filename;
    fs.readFile( file , function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
});


router.get('/fetchDocument/:documentId', function(req, res){
    if(!req.params.documentId ){
        res.status(400);
        res.json({ "error": "Bad Data" });
    } else {
        db.signdesk_documents.findOne( {_id: mongojs.ObjectId( req.params.documentId )}  , function(err, documentInfo){
            if(err){ res.send(err); }
            res.json(documentInfo);
        });
    }
});


router.get('/fetchDocumentSigner/:documentId', function(req, res){

    var finalResultList = []
    if(!req.params.documentId ){
        res.status(400);
        res.json({ "error": "Bad Data" });
    } else {
        db.signdesk_signers.find( {document_id:req.params.documentId}  , function(err, signerList ){
            if(err){ //res.send(err);
            }


            var user_ids = signerList.map(function(signer) { return  mongojs.ObjectId( signer.user_id ) });

            // perform query on user table: find all users for which we have a bag
            db.signdesk_users.find({ _id : { $in : user_ids } }, function(err, users) {
                // create a mapping of username -> first name for easy lookup
                var usernames = [];
                users.forEach(function(user) {
                    usernames[user._id.toString()] = user.profile.first_name;
                });

                // map first names to bags
                signerList.forEach(function(signer) {
                    signer.name = usernames[signer.user_id];

                });

                // done: return it as JSON (no need to build a JSON string ourselves)
                res.send(signerList);
            });


        });
    }
});


router.post('/getSigned', function(req, res, next){

    try{


        var documentInfo = req.body;

        console.log('documentInfo',documentInfo)

        var exec = require('child_process').exec,child;

         child = exec('java -jar dist/eSign.jar '+documentInfo.file+' '+documentInfo.outputFile+' '+documentInfo.aadhaar+' Y '+documentInfo.otp+' '+documentInfo.transactionId+' '+documentInfo.referenceId, function(error,stdout,stderr){
             if(stdout){
                 console.log('stdout',stdout)
                 res.status(200);
                 res.json(stdout);
             }

             if(!stdout && stderr){
                 console.log('stderr',stderr)
                 res.status(200);
                 res.send(stderr);
             }

             /*if(error){
                 res.status(400);
                 res.send(error);
             }*/
         });

    }catch(e){

        res.status(200);
        res.send('es_111');
    }
});



router.post('/createDocument', function(req, res, next){
    var documentInfo = req.body;

    if(!documentInfo.document_name || !documentInfo.document_title  || !documentInfo.document_source ){
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.signdesk_documents.save(documentInfo, function(err, document){
            if(err){ res.send(err); }
            res.json(document);
        });
    }
});

router.post('/notifySignerByOrder', function(req, res, next){

    var posted_data = req.body;


    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: emailUser+'@legaldesk.com',
            pass: auth
        }
    });

    db.signdesk_signers.findOne( { $query: {document_id:posted_data.documentId,email_notification:false}, $orderby: { signature_sequence : 1 } } , function(err, signerInfo){

        if(err){ console.log('Error on /notifySignerByOrder section ') }

        if(signerInfo){


            console.log('signerInfo',signerInfo)
            db.signdesk_users.findOne( {_id: mongojs.ObjectId( signerInfo.user_id )}  , function(err, userInfo){

                console.log('sending email',userInfo.email_id)
                // setup email data with unicode symbols
                let mailOptions = {
                    from: '"No-Reply "<no-reply@legaldesk.com>', // sender address
                    to: userInfo.email_id, // list of receivers
                    subject: 'You are invited to eSign', // Subject line
                    //text: 'Hello world ?', // plain text body
                    html: '<div>Dear user <br/>Greetings from SignDesk!<br/>SignDesk has invited you to sign a document electronically. Kindly click on the link below to eSign the document.<br/><a href="http://localhost:4200/#/online/view/'+posted_data.documentId+'/'+signerInfo._id.toString()+'">Sign Document</a></div>' // html body
                };
                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {  return console.log(error);  }


                     signerInfo.email_notification =true
                    db.signdesk_signers.update({_id: mongojs.ObjectId(signerInfo._id.toString())},signerInfo, {}, function(err, task){
                        if(err){
                            res.send(err);
                        }
                        res.json({status:"success"});
                    });

                });


            });

        }else{
            //all invited
        }
    });





});



router.post('/updateFilename', function(req, res, next){
    var posted_data = req.body;
    if(!posted_data.documentId || !posted_data.filename  ){
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    }else{

        db.signdesk_documents.update({_id: mongojs.ObjectId(posted_data.documentId )},  {$set:{'latest_document': posted_data.filename }}, function (err, document) {
            if(err){ res.send(err); }
            console.log(document)
            res.json(document);
        });

    }
});



router.post('/addSigner', function(req, res, next){
    var posted_data = req.body;

    if(!posted_data.signer_name || !posted_data.signer_email  || !posted_data.signerInfo ){
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    }else{

        db.signdesk_users.findOne({email_id: posted_data.signer_email}, function(err, userInfo){
            if(err){ res.send(err); }
            if(userInfo){
                posted_data.signerInfo.user_id= userInfo._id

                db.signdesk_signers.save(posted_data.signerInfo, function(err, document){
                    if(err){ res.send(err); }
                    res.json({
                        _id      : document._id.toString(),
                        name     : posted_data.signer_name,
                        emailId  : posted_data.signer_email,
                        mobile   : posted_data.signer_mobile
                    });
                });
                console.log('user  found');
            }else{

                var newUser = {
                    organisation_id : "org_1",
                    department_id   : "dep_1",
                    email_id        : posted_data.signer_email,
                    mobile_number   : posted_data.signer_mobile,
                    password        : 'password',
                    address         : [],
                    roleId          : "level1",
                    profile         : {
                        first_name  : posted_data.signer_name,
                        last_name   : ''
                    },
                    preferences     : {},
                    access_logs     : {},
                    created_by      : 'user_1',
                    created_at      : new Date(),
                    type            : 'external'
                }
                db.signdesk_users.save(newUser, function(err, insertedUser){
                    if(err){ res.send(err); }
                    posted_data.signerInfo.user_id= insertedUser._id.toString()
                    db.signdesk_signers.save(posted_data.signerInfo, function(err, document){
                        if(err){ res.send(err); }
                        res.json({
                            _id      : document._id.toString(),
                            name     : posted_data.signer_name,
                            emailId  : posted_data.signer_email,
                            mobile   : posted_data.signer_mobile
                        });
                    });
                });
            }
        });

    }
});




router.post('/importSigner', function(req, res, next){
    var importInfo = req.body;
    console.log('importInfo',importInfo)
    var result = [];
    var eachRow =[];
    if(!importInfo.filename    ){

        res.status(400);
        res.json({
            "error": "Bad Data"
        });

    } else {

         xls('./uploads/import-signer/'+importInfo.filename, function(err, data) {
             if(err) throw err;

             var signerInfo = {
                 document_id        : importInfo.documentId,
                 external_user      : false,
                 user_id            : '',
                 email_notification : false,
                 signature_page     :'all' ,
                 signature_position : {
                         page       : 'all',
                         position   : 'bottom-left'
                 },
                 status             : "invited",
                 invited_at         : new Date(),
                 invited_by         : 'user_1',
                 remainders         : {}
             }

             function insertSigner (counter, data){
                 if(!counter) counter =1

                 console.log(counter+'counter',data.length);
                 if(counter >=data.length)  {
                     res.json(result);
                     return
                 }

                 signerInfo.signer_index= counter;
                 signerInfo.signature_sequence= counter;
                 eachRow = data[counter];


                 if(eachRow[0].toString().trim()!="" && eachRow[1].toString().trim()!=""  && eachRow[2].toString().trim()!=""){


                     db.signdesk_users.findOne({email_id:eachRow[1]}, function(err, userInfo){

                         if(err){  }

                         if(userInfo){
                             signerInfo.user_id= userInfo._id.toString()

                             signerInfo._id =null;
                             db.signdesk_signers.insert(signerInfo, function(err, resp){
                                 if(err){ console.log('err',err)}

                                 result.push({
                                     _id      : resp._id.toString(),
                                     name     : eachRow[0],
                                     emailId  : eachRow[1],
                                     mobile   : eachRow[2]
                                 });
                                 insertSigner(++counter,data);
                             });

                         }else{

                             var newUser = {
                                 organisation_id : "org_1",
                                 department_id   : "dep_1",
                                 email_id        : eachRow[1],
                                 mobile_number   :eachRow[2],
                                 password        : 'password',
                                 address         : [],
                                 roleId          : "level1",
                                 profile         : {
                                     first_name  : eachRow[0],
                                     last_name   : ''
                                 },
                                 preferences     : {},
                                 access_logs     : {},
                                 created_by      : 'user_1',
                                 created_at      : new Date(),
                                 type            : 'external'
                             }
                             db.signdesk_users.save(newUser, function(err, insertedUser){
                                 if(err){ res.send(err); }

                                 signerInfo.user_id= insertedUser._id.toString()
                                 signerInfo._id =null;
                                 db.signdesk_signers.save(signerInfo, function(err, resp){
                                     if(err){ }
                                     result.push({
                                         _id      : resp._id.toString(),
                                         name     : eachRow[0],
                                         emailId  : eachRow[1],
                                         mobile   : eachRow[2]
                                     });
                                     insertSigner(++counter,data);
                                 });


                             });

                             console.log('user not found');
                         } //end if
                     });

                 }else{
                     insertSigner(++counter,data);
                 }
             }

             insertSigner (1, data)

         });
    }
});


router.get('/getDocuments/', function(req, res){
    var finalResult = [];
    db.signdesk_documents.find(function(err, signdesk_documents){
        if(err){  res.send(err); }
        finalResult =signdesk_documents.map(function(document){
            return {
                'signDeskDocument':document,
                'signersCount':db.signdesk_signers.find({document_id : document._id.toString()}).count(function(error, nbDocs) {
                 return nbDocs })
            };
        });

        res.json(finalResult);
    });
});



router.delete('/deleteSigner/:id', function(req, res, next){
    db.signdesk_signers.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, signer){
        if(err){
            res.send(err);
        }
        res.json(signer);
    });
});

router.post('/sendEmail', function(req, res, next){
    var posted_data = req.body;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: emailUser+'@legaldesk.com',
            pass: auth
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"No-Reply "<no-reply@legaldesk.com>', // sender address
        to: 'gururaj.shetty@legaldesk.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        //text: 'Hello world ?', // plain text body
        html: '<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

});



var excelstorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, '../uploads/import-signer/');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '_' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});
var excelupload = multer({ //multer settings
    storage: excelstorage
}).single('file');
router.post('/excelUpload', function(req, res) {
    excelupload(req,res,function(err){
        // console.log(req.file);
        if(err){
            res.json({error_code:1,err_desc:err});
            return;
        }



        res.json({error_code:0,err_desc:null,fileInfo:req.file});
    });
});



module.exports = router;